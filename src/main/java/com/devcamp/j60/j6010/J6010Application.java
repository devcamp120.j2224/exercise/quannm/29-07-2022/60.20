package com.devcamp.j60.j6010;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J6010Application {

	public static void main(String[] args) {
		SpringApplication.run(J6010Application.class, args);
	}

}

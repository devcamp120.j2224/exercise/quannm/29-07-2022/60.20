package com.devcamp.j60.j6010.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.j6010.model.CRegion;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {

}

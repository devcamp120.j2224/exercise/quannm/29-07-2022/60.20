package com.devcamp.j60.j6010.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.j6010.model.CCountry;


public interface ICountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
}
